package com.epam.view;

import com.epam.games.LongestPlateau;
import com.epam.games.Minesweeper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

public class View {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final Scanner SCANNER = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private LongestPlateau plateau;
    private Minesweeper minesweeper;

    public View() {
        menu = new LinkedHashMap<>();
        menu.put("1", "Play longest plateau");
        menu.put("2", "Play Minesweeper");
        menu.put("Q", "Exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::findLongestPlateau);
        methodsMenu.put("2", this::playMinesweeper);
        methodsMenu.put("Q", this::quit);
    }

    private void showMenu() {
        menu.forEach((key, value) -> LOGGER.info(key + " - " + value + "\n"));
    }

    private void findLongestPlateau() {
        LOGGER.info("Enter size of array: ");
        int n = SCANNER.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = SCANNER.nextInt();
        }
        plateau = new LongestPlateau(array);
        if (plateau.getLength() == 0) {
            LOGGER.info("There is no plateaus!");
        } else {
            LOGGER.info("The longest plateau is: ");
            for (int i = plateau.getStart(); i < plateau.getEnd(); i++) {
                LOGGER.info(array[i] + " ");
            }
        }
    }

    private void playMinesweeper() {
        LOGGER.info("Enter size of field (MxN): ");
        int m = SCANNER.nextInt();
        int n = SCANNER.nextInt();
        LOGGER.info("Enter probability of mines occurrence: ");
        double p = SCANNER.nextDouble();
        minesweeper = new Minesweeper(m, n, p);
        Arrays.stream(minesweeper.getSolution())
                .map(a -> Arrays.stream(a).mapToObj(String::valueOf).collect(Collectors.joining(" ")))
                .forEach(s -> LOGGER.info(s + "\n"));
    }

    private void quit() {
    }

    public void show() {
        String key;
        do {
            showMenu();
            LOGGER.info("Enter menu point: ");
            key = SCANNER.nextLine().toLowerCase();
            if (methodsMenu.containsKey(key)) {
                methodsMenu.get(key).print();
            }
        } while (key.equals("q"));
    }
}
