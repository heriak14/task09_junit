package com.epam.games;

import static org.junit.Assert.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class LongestPlateauTest {

    int[] testArray1 = {1, 2, 2, 0, 4, 5, 5, 5, 5, 7};
    @Mock
    Findable findable;

    @InjectMocks
    LongestPlateau plateau = new LongestPlateau(testArray1);

    @Test
    void testFind() {
        when(findable.find(testArray1)).thenReturn(new int[]{5, 9, 4});
        assertArrayEquals(new int[]{5, 9, 4}, plateau.findViaFindable());
    }

    @Test
    void testFindPlateau() {
        assertSame(4, plateau.getLength());
        assertSame(5, plateau.getStart());
        assertSame(9, plateau.getEnd());
    }
}
